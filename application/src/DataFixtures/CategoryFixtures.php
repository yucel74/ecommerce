<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private $price = 500;

    public function load(ObjectManager $manager)
    {
        //create fake data Category
        for($i=1; $i<5; $i++){
            $categorie = new Category;
            $categorie->setName("Categorie n° $i");
            $manager->persist($categorie);

            $this->price++;
            $product = new Product;
            $product->setCategory($categorie)
                    ->setName("Produit n° $i")
                    ->setSlug("Slug du produit $i")
                    ->setIllustration("http://placehold.it/250x200")
                    ->setSubtitle("description courte du produit n° $i")
                    ->setDescription("Description produit n° $i Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sapiente eius amet perspiciatis, quam provident beatae. Unde accusamus nam sunt odio ut aspernatur aliquam molestiae obcaecati, iste ipsam facilis eveniet cumque. ")
                    ->setPrice($this->price)
                    ->setIsBest(0);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
