<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

class Mail
{
    private $apiKey = '15fbd6dbc0c129e11e63cc22187e4806';

    private $apiKeySecret = 'd5198ecc05cd37924c4c0541559cd9a9';

    /**
     * @param string $toEmail
     * @param string $toName
     * @param string $subject
     * @param string $content
     *
     * @return bool
     */
    public function send(string $toEmail, string $toName, string $subject, string $content): bool
    {
        $mj = new Client($this->apiKey, $this->apiKeySecret, true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "contact@clothes-project.com",
                        'Name' => "Clothes.com"
                    ],
                    'To' => [
                        [
                            'Email' => $toEmail,
                            'Name' => $toName
                        ]
                    ],
                    'TemplateID' => 6138296,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content
                    ]
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        return $response->success();
    }
}