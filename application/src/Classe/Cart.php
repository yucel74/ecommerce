<?php

namespace App\Classe;

use App\Service\ProductService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class Cart
{
    private $session;

    private $entityManager;

    private $productService;

    public function __construct(
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        ProductService $productService
    )
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->productService = $productService;
    }
    
    public function add(int $id)
    {
        $cart = $this->session->get('cart', []);
        $product = $this->productService->getProduct($id);

        if ($product) {

            if (!empty($cart[$id])) {
                $cart[$id] += 1;
            } else {
                $cart[$id] = 1;
            }

        } else {
            $this->delete($id);
        }

        $this->session->set('cart', $cart);
    }

    public function addSinceProduct(int $id , int $quantity)
    {
        $cart = $this->session->get('cart', []);

        if (!empty($cart[$id])) {
            $cart[$id] += $quantity;
        } else {
            $cart[$id] = 1;
        }

        $this->session->set('cart', $cart);
    }

    public function get()
    {
        return $this->session->get('cart');
    }

    public function remove()
    {
        return $this->session->remove('cart');
    }

    public function delete(int $id)
    {
        $cart = $this->session->get('cart', []);
        unset($cart[$id]);

        return $this->session->set('cart', $cart);
    }


    public function decrease(int $id)
    {
        $cart = $this->session->get('cart', []);

        if ($cart[$id] > 1){
            $cart[$id]--;
        } else {
            unset($cart[$id]);
        }
    
        return $this->session->set('cart', $cart);
    }

    public function getFull() {
        $cartComplete = [];

        if ($this->get()){
            foreach ($this->get() as $id => $quantity) {
                $product_object = $this->productService->getProduct($id);
                //to prevent someone from having fun typing any id on the cart/add/{id} url, 
                //if you type anything as the id number and therefore the product 
                //does not exist it will not be added to the cart list         
             
                if (!$product_object) {
                    $this->delete($id);
                    continue;
                }
                
                $cartComplete[] = [
                    'product' => $product_object,
                    'quantity' => $quantity
                ];
            }
        }
        return $cartComplete;
    }
}