<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ExceptionListener extends AbstractController
{

    public function onKernelException(ExceptionEvent $event)
    {

        // You get the exception object from the received event
        $exception = $event->getThrowable();
        
        $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent($message);

        $getCode = null;
        $getHeader = null;
        $t = null;

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {

            $getCode = $exception->getStatusCode();
            $getHeader = $exception->getHeaders();
            
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

       
        switch ($getCode) {

            case 404:

                $t = $this->render("bundles/TwigBundle/Exception/error$getCode.html.twig", ["code" => $getCode , "header" => $getHeader]);
                break;
            
            case 403:
                $t = $this->render("bundles/TwigBundle/Exception/error$getCode.html.twig", ["code" => $getCode , "header" => $getHeader]);
                break;
            
            default:

                $t = $this->render("bundles/TwigBundle/Exception/error.html.twig", ["code" => $getCode , "header" => $getHeader]);
               
                break;
        }
    
    
        $event->setResponse($t);
    }
}