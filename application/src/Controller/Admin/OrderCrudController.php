<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\OrderDetails;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use phpDocumentor\Reflection\Types\Boolean;


class OrderCrudController extends AbstractCrudController
{
    private $entityManager;
    //this variable will generate the route redirection once the
    // "updatePreparation" function is performed
    private $crudUrlGenerator;

    public function __construct(EntityManagerInterface $entityManager, CrudUrlGenerator $crudUrlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    //Allows you to view the order
    public function configureActions(Actions $actions): Actions
    {
        $updatePreparation =
            Action::new('updatePreparation', '  Préparation en cours','fas fa-box-open')
            ->linkToCrudAction('updatePreparation');
        $updateDelivery =
            Action::new('updateDelivery', 'Livraison en cours  .','fas fa-truck')
            ->linkToCrudAction('updateDelivery');
        $orderDelivered =
            Action::new('orderDelivered', 'Commande livrée  .','fas fa-check-circle')
            ->linkToCrudAction('orderDelivered');
        
        return $actions
            ->add('detail', $updatePreparation)
            ->add('detail', $updateDelivery)
            ->add('detail', $orderDelivered)
            ->add('index', 'detail');
    }

    public function updatePreparation(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(Order::IN_PREPARATION);
        $this->entityManager->flush();

        $this->addFlash('notice', "<span style='color:green;'><strong>La commande".$order->getReference()." est bien <u>en cours de préparation.</u></strong></span>");
        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        
            //notif mail possible
        return $this->redirect($url);
    }

    public function updateDelivery(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(Order::SHIPPING_IN_PROGRESS);
        $this->entityManager->flush();

        $this->addFlash('notice', "<span style='color:green;'><strong>La commande".$order->getReference()." est bien <u>en cours de livraison.</u></strong></span>");
        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        
            //notif mail possible
        return $this->redirect($url);
        
    }

    public function orderDelivered(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(Order::DELIVERED);
        $this->entityManager->flush();

        $this->addFlash('notice', "<span style='color:green;'><strong>La commande".$order->getReference()." a bien été livrée.</u></strong></span>");
        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        
            //notif mail possible
        return $this->redirect($url);
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            DateTimeField::new('createdAt', 'Passée le '),
            TextField::new('user.fullname', 'Utilisateur'),
            TextEditorField::new('delivery', 'Adresse de livraison')->onlyOnDetail(),
            MoneyField::new('total')->setCurrency('EUR'),
            TextField::new('carrierName', 'Transporteur'),
            MoneyField::new('carrierPrice', 'Frais de port')->setCurrency('EUR'),
            ChoiceField::new('state')->setChoices(array_flip(Order::VALUE_LABEL_STATUSES)),
            ArrayField::new('OrderDetails', 'Produits achetés')->hideOnIndex()
        ];
    }
}


