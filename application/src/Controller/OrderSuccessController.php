<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\Mail;
use App\Entity\Order;
use App\Service\MailService;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderSuccessController extends AbstractController
{
    private $entityManager;

    private $orderService;

    public function __construct(
        EntityManagerInterface $entityManager,
        OrderService $orderService
    )
    {
        $this->entityManager = $entityManager;       
        $this->orderService = $orderService;
    }

    /**
     * validate Order after pay
     * @Route("/commande/merci/{stripeSessionId}", name="order_validate")
     */
    public function index(Cart $cart, string $stripeSessionId, MailService $mailService): Response
    {
        $order = $this->orderService->findOneByStripeSessionId($stripeSessionId);

        if (!$order || $order->getUSer() != $this->getUser() ){
            return $this->redirectToRoute('home');
        }

        if ($order->getState() === Order::IS_NOT_PAY){
            //empty the shopping cart session
            $cart->remove();

            $order->setState(Order::IS_PAY);
            $this->entityManager->flush();

            $mailService->sendOrderSuccess($this->getUser());
        }

        return $this->render('order_success/index.html.twig', [
            'order' => $order
        ]);
    }
}