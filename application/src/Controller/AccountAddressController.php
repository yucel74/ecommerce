<?php

namespace App\Controller;

use App\Entity\Address;
use App\Classe\Cart;
use App\Service\AddressService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\AddressType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class AccountAddressController extends AbstractController
{
    private $entityManager;

    private $addressService;

    public function __construct(
        EntityManagerInterface $entityManager,
        AddressService $addressService
    )
    {   
        $this->entityManager = $entityManager;
        $this->addressService = $addressService;
    }

    /**
     * @Route("/compte/adresses", name="account_address")
     */
    public function index(): Response
    {        
        return $this->render('account/address.html.twig');
    }

    /**
     * @Route("/compte/ajouter-une-adresse", name="account_address_add")
     */
    public function add(Cart $cart, Request $request): Response
    {
        $address = new Address;
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request); 

        if ($form->isSubmitted() && $form->isValid())
        {
            $address->setUser($this->getUser());
            $this->entityManager->persist($address);
            $this->entityManager->flush();
            
            if ($cart->get())
            {
                return $this->redirectToRoute('order');
            } else {
                return $this->redirectToRoute('account_address');
            }
        }

        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/modifier-une-adresse/{id}", name="account_address_edit")
     */
    public function edit(Request $request, int $id): Response
    {
        $address = $this->addressService->getAddress($id);

        if (!$address || $address->getUser() != $this->getUser())
        {
            return $this->redirectToRoute('account_address');
        }
        
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request); 

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->entityManager->flush();
            return $this->redirectToRoute('account_address');
        }

        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/supprimer-une-adresse/{id}", name="account_address_delete")
     */
    public function delete(int $id): Response
    {
        $address = $this->addressService->getAddress($id);

        if ($address && $address->getUser() == $this->getUser())
        {
            $this->entityManager->remove($address);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('account_address');
    }
}