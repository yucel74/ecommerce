<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class AccountPasswordController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/compte/modifier-mon-mot-de-passe", name="account_password")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $notification = null;
        $alert = null;

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            //retrieves the current password that the user entered
            $old_pwd = $form->get('old_password')->getData();
            
            //checks if the current password is the same in the database
            if ($encoder->isPasswordValid($user, $old_pwd))
            {
                //retrieves the new password that the user entered
                $new_pwd = $form->get('new_password')->getData();
                // password hashing
                $password = $encoder->encodePassword($user, $new_pwd);

                $user->setPassword($password);
                $this->entityManager->flush();

                $notification = "Votre mot de passe à bien été mis à jour.";
                $alert = 'alert alert-success';
            } else {
                $notification = "Votre mot de passe actuel n'est pas le bon.";
                $alert = 'alert alert-danger';
            }
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification,
            'alert' => $alert
        ]);
    }
}