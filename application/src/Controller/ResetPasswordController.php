<?php

namespace App\Controller;

use App\Form\ResetPasswordType;
use App\Service\MailService;
use App\Service\ResetPasswordService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends AbstractController
{
    private $entityManager;

    private $userService;

    private $resetPasswordService;

    private $mailService;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserService $userService,
        ResetPasswordService $resetPasswordService,
        MailService $mailService
    )
    {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
        $this->resetPasswordService = $resetPasswordService;
        $this->mailService = $mailService;
    }

    /**
     * @Route("/mot-de-passe-oublie", name="reset_password")
     */
    public function index(Request $request): Response
    {
        if ($this->getUser())
        {
            return $this->redirectToRoute('home');
        }

        if ($request->get('email'))
        {
            //check if the user exists
            $user = $this->userService->getUserByEmail($request->get('email'));
            if ($user)
            {
                //saves the password reset request in the database. user, token, createdAt
                $resetPassword = $this->resetPasswordService->createResetPassword($user);

                $resetPasswordUrl = $this->generateUrl('update_password', [
                    'token' => $resetPassword->getToken()
                ]);

                $content = $this->resetPasswordService->getPasswordResetRequestMessage(
                    $user->getFirstName(), $resetPasswordUrl
                );

                $this->mailService->sendResetPassword($user, $content);

                $this->addFlash(
                    'notice',
                    'Vous allez recevoir dans quelques secondes un mail avec la procédure pour réinitialiser votre mot de passe'
                );
            }
            else
            {
                $this->addFlash(
                    'notice',
                    'Cette adresse email est inconnue.'
                );
            }
        }

        return $this->render('reset_password/index.html.twig');
    }

    /**
     * @Route("/modifier-mon-mot-de-passe/{token}", name="update_password")
     */
    public function update(Request $request, string $token, UserPasswordEncoderInterface $encoder): Response
    {
        $resetPassword = $this->resetPasswordService->getByToken($token);

        if (!$resetPassword)
        {
            return $this->redirectToRoute('reset_password');
        }

        //Timing for reset password
        $now = new \DateTime;
        if ($now > $resetPassword->getCreatedAt()->modify('+3 hour'))
        {
            $this->addFlash(
                'notice',
                'Votre demande de mot de pase a expiré.Merci de renouveller votre demande.'
            );

            return $this->redirectToRoute('reset_password');
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // Encoding password
            $newPassword = $form->getData()['new_password'];
            $password = $encoder->encodePassword($resetPassword->getUser(), $newPassword);
            $resetPassword->getUser()->setPassword($password);

            //Flush in database
            $this->entityManager->flush();

            $this->addFlash(
                'notice',
                'Votre mot de passe a bien été mis à jour.'
            );

            return $this->redirectToRoute('app_login');
        }

        return $this->render('reset_password/update.html.twig', [
            'form' => $form->createView()
        ]);
    }
}