<?php

namespace App\Controller;

use App\Entity\Order;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountOrderController extends AbstractController
{
    private $entityManager;

    private $orderService;

    public function __construct(
        EntityManagerInterface $entityManager,
        OrderService $orderService
    )
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * @Route("/compte/mes-commandes", name="account_order")
     */
    public function index(): Response
    {
        $orders = $this->orderService->findSuccessOrders($this->getUser());

        return $this->render('account/order.html.twig', [
            'orders' => $orders,
            'orderStatuses' => Order::VALUE_LABEL_STATUSES
        ]);
    }

    /**
     * @Route("/compte/mes-commandes/{reference}", name="account_order_show")
     */
    public function show(string $reference): Response
    { 
        $order = $this->orderService->findOneByReference($reference);

        if (!$order || $order->getUser() != $this->getUser())
        {
            return $this->redirectToRoute('account_order');
        }

        return $this->render('account/order_show.html.twig', [
            'order' => $order,
            'orderStatuses' => Order::VALUE_LABEL_STATUSES
        ]);
    }
}