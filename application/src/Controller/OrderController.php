<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Form\OrderType;
use App\Repository\CarrierRepository;
use App\Service\OrderService;
use App\Service\OrderDetailsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $entityManager;

    private $orderService;

    private $orderDetailsService;

    public function __construct(
        EntityManagerInterface $entityManager,
        OrderService $orderService,
        OrderDetailsService $orderDetailsService
    )
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
        $this->orderDetailsService = $orderDetailsService;
    }

    /**
     * @Route("/commande", name="order")
     */
    public function index(Cart $cart): Response
    {
        if (!$this->getUser()){
            return $this->redirectToRoute('app_login');
        }

        // checks if the logged in user has not yet added an address
        if (!$this->getUser()->getAddresses()->getValues()){
            return $this->redirectToRoute('account_address_add');
        }

        if (empty($cart->getFull())){
            return $this->redirectToRoute('home');
        }

        // retrieval of the form allowing the logged in user to indicate
        // delivery address and transport provider
        $form = $this->createForm(OrderType::class, null, [
            'user'=> $this->getUser()
        ]);

        return $this->render('order/index.html.twig', [
            'form' => $form->createView(),
            'cart' => $cart->getFull()
        ]);
    }

    /**
     * @Route("/commande/recapitulatif", name="order_recap", methods={"POST"})
     */
    public function add(Cart $cart, Request $request, CarrierRepository $carrierRepository): Response
    {
        $form = $this->createForm(OrderType::class, null, [
            'user'=> $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $carrier = $form->get('carrier')->getData();
            $address = $form->get('addresses')->getData();

            $error = $this->orderService->isValidOrderInformations($carrier, $address);

            if (null !== $error)
            {
                $this->addFlash(
                    'error',
                    $error
                );
            }
            $deliveryMessage = $this->orderService->getDeliveryMessage($address);
            $order = $this->orderService->createOrder($this->getUser(), $carrier, $deliveryMessage);

            foreach ($cart->getFull() as $productDetails) {
                $this->orderDetailsService->createOrderDetails($order, $productDetails);
            }
            
            $this->entityManager->flush();
            
            return $this->render('order/add.html.twig', [
                'cart' => $cart->getFull(),
                'carrier' => $carrier,
                'delivery' => $deliveryMessage,
                'reference' => $order->getReference()
            ]);
        } else {
            $this->addFlash(
                'error',
                'Une erreur s\'est produite'
             );
        }

        return $this->redirectToRoute('cart');
    }
}