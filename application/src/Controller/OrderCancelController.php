<?php

namespace App\Controller;

use App\Entity\Order;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderCancelController extends AbstractController
{
    private $entityManager;

    private $orderService;

    public function __construct(
        EntityManagerInterface $entityManager,
        OrderService $orderService
    )
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * @Route("/commande/erreur/{stripeSessionId}", name="order_cancel")
     */
    public function index(string $stripeSessionId): Response
    {
        $order = $this->orderService->findOneByStripeSessionId($stripeSessionId);

        if (!$order || $order->getUSer() != $this->getUser() ){
            return $this->redirectToRoute('home');
        }

        return $this->render('order_cancel/index.html.twig', [
            'order' => $order
        ]);
    }
}