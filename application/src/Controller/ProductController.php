<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Repository\ProductRepository;
use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use App\Service\CartService;
use App\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends AbstractController
{
    private $entityManager;
    private $productService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductService $productService
    )
    {
        $this->entityManager = $entityManager;
        $this->productService = $productService;
    }

    /**
     * @Route("/nos-produits", name="products")
     */
    public function index( Request $request ,ProductRepository $productRepository): Response
    {    
        //instantiation of a new search
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            //search based on retrieved values
            $products = $productRepository->findWithSearch($search);
        } else {
            //if the filter form is not used, sends all products by default
            $products = $this->productService->getProducts();
        }

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/produit/{id}", name="product")
     */
    public function show(Request $request, int $id, Cart $cart, CartService $cartService): Response
    {
        $product = $this->productService->getProduct($id);

        //checking if the product exists in the database
        if(!$product)
        {
            return $this->redirectToRoute('products');
        }
        
        $quantityProduct = $request->request->get('quantityProduct');
        
        if($request->isMethod('POST')) {
            $quantityProduct = abs(intval($quantityProduct));
            $cart->addSinceProduct($id, $quantityProduct);
            
            return $this->redirectToRoute('product', ['id' => $id]);
        }
        
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $this->productService->getBestSelling(Product::IS_BEST_SELL),
            'productCartCount' => $cartService->getProductCount($product,$cart)
        ]);
    }
}