<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ResetPassword;
use App\Service\HeaderService;
use App\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $entityManager;

    private $productService;

    private $headerService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductService $productService,
        HeaderService $headerService
    )
    {
        $this->entityManager = $entityManager;        
        $this->productService = $productService;
        $this->headerService = $headerService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'products' => $this->productService->getBestSelling(Product::IS_BEST_SELL),
            'headers' => $this->headerService->getHeaders()
        ]);
    }
}