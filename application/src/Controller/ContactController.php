<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Form\ContactType;
use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Request $request, MailService $mailService): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // notification
            $this->addFlash(
               'notice',
               'Merci de nous avoir contacté. Notre équipe va vous répondre dans les meilleures délais.'
            );

            $mailService->sendContact($form->getData());
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}