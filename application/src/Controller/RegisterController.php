<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use App\Service\MailService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private $entityManager;

    private $userService;

    private $mailService;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserService $userService,
        MailService $mailService
    )
    {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
        $this->mailService = $mailService;
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $notification = null;

        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        //listen to the object $form to know if a post has been made
        $form->handleRequest($request);

        //if the form is submitted and valid
        if ($form->isSubmitted() && $form->isValid())
        {
            //checks if the mail already exists in the database
            $search_email = $this->userService->getUserByEmail($user->getEmail());

            //if the mail does not exist
            if (!$search_email)
            {
                //the password entered by the user is hash
                $password = $encoder->encodePassword($user,$user->getPassword());
                //change the password property
                $user->setPassword($password);
                //freezes the data to save it
                $this->entityManager->persist($user);
                //registration of the frozen object in the database
                $this->entityManager->flush();
                //if the object is saved, a notification is made to the user
                $notification = "Votre inscription s'est bien déroulée. Vous pouvez dès à présent vous connecter à votre compte.";

                $this->mailService->sendRegister($user);
            } else {
                //if the object not saved, a notification is made to the userot
                $notification = "L'email que vous avez renseigné est déjà utilisé.";                
            }
        }

        //transmission of data to the template
        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }
}