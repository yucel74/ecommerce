<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Service\OrderService;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Controller\Response;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    private $entityManager;

    private $productService;

    private $orderService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductService $productService,
        OrderService $orderService
    )
    {
        $this->entityManager = $entityManager;
        $this->productService = $productService;
        $this->orderService = $orderService;
    }

    /**
     * @Route("/commande/create-session/{reference}", name="stripe_create_session")
     */
    public function index(string $reference): JsonResponse
    {
        $products_for_stripe = [];
        $YOUR_DOMAIN = 'http://127.0.0.1:8741';

        $order = $this->orderService->findOneByReference($reference);

        if (!$order){
            new JsonResponse(['error' => 'order']);
        }

        foreach ($order->getOrderDetails()->getValues() as $product) {
            $product_object = $this->productService->getProductByName($product->getProduct());
            $products_for_stripe []= [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $product->getPrice(),
                    'product_data' => [
                        'name' => $product->getProduct(),
                        'images' => [$YOUR_DOMAIN."/uploads/".$product_object->getIllustration()],
                    ],
                ],
                'quantity' => $product->getQuantity(),
                
            ];
        }

        $products_for_stripe []= [
            'price_data' => [
                'currency' => 'eur',
                'unit_amount' => $order->getCarrierPrice(),
                'product_data' => [
                    'name' => $order->getCarrierName(),
                    'images' => [$YOUR_DOMAIN],
                ],
            ],
            'quantity' => 1,
        ];
        
        Stripe::setApiKey('sk_test_51J7hy6LhozwyJPe8QivdiwXiwJZZzSMCAQUXqVwdvxFG69Hyp6RFj59k8mKaOYkdVlE17N9YJJemtV02Y0h5Bj2000GheQJPbV');

        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [
                $products_for_stripe
            ],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN . '/commande/merci/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $YOUR_DOMAIN . '/commande/erreur/{CHECKOUT_SESSION_ID}',
        ]);

        $order->setStripeSessionId($checkout_session->id);
        $this->entityManager->flush();
        $response = new JsonResponse(['id' => $checkout_session->id]);

        return $response;
    }
}