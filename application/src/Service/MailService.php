<?php

namespace App\Service;

use App\Classe\Mail;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface;

class MailService {

    /**
     * @param User   $user
     * @param string $subject
     * @param string $content
     *
     * @return bool
     */
    public function sendMail(User $user, string $subject, string $content): bool
    {
        $mail = new Mail();
        return $mail->send(
            $user->getEmail(),
            $user->getFirstname() . ' ' . $user->getLastname(),
            $subject,
            $content
        );
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function sendRegister(User $user): bool
    {
        $content = 'Bonjour '.$user->getFirstname().'<br/> Bienvenue sur notre boutique. 
            <br/><br/> Votre compte a bien été enregistré, Félicitations!<br/><br/>
            L\'équipe Clothes.com';

        return $this->sendMail($user, 'Bienvenue sur Clothes.com', $content);
    }

    /**
     * @param User   $user
     * @param string $content
     *
     * @return bool
     */
    public function sendResetPassword(User $user, string $content): bool
    {
        return $this->sendMail($user, 'Réinitialiser votre mot de passe sur Clothes.com', $content);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function sendContact(array $data): bool
    {
        $content = "Vous avez reçu un mail de la part de: ". $data['lastname'] . " ".$data['firstname'] ."<br/> ";
        $content .= "Objet du message: <br/>". $data['object']."<br/>";
        $content .= "Contenu du message: <br/>".$data['content']."<br/>";
        $content .= "Email de l'auteur du message: <br/>". $data['mail'];

        $mail = new Mail();
        return $mail->send('contact@clothes-project.com', 'Boutique de vetements', 'Mail reçu depuis le formulaire de contact', $content);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function sendOrderSuccess(User $user): bool
    {
        $content = "Bonjour ". $user->getFirstname()."<br/> Confirmation de commande sur Clothes.com. <br/><br/> Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque iure doloremque numquam laboriosam animi. Repellendus nisi pariatur ratione, eius architecto adipisci, libero commodi fugiat illum inventore optio, enim veritatis suscipit.";

        return $this->sendMail($user, 'Votre commande Clothes.com a été validé', $content);
    }
}