<?php

namespace App\Service;

use App\Entity\Address;
use App\Entity\Carrier;
use App\Entity\Order;
use App\Entity\User;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;

class OrderService {

    private $orderRepository;

    private $entityManager;

    public function __construct(
        OrderRepository $orderRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->orderRepository = $orderRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function findSuccessOrders(User $user): array
    {
        return $this->orderRepository->findSuccessOrders($user);
    }

    /**
     * @param string $reference
     *
     * @return Order|null
     */
    public function findOneByReference(string $reference): ?Order
    {
        return $this->orderRepository->findOneByReference($reference);
    }

    /**
     * @param string $stripeSessionId
     *
     * @return Order|null
     */
    public function findOneByStripeSessionId(string $stripeSessionId): ?Order
    {
        return $this->orderRepository->findOneByStripeSessionId($stripeSessionId);
    }

    /**
     * @param User    $user
     * @param Carrier $carrier
     * @param string  $deliveryContent
     *
     * @return Order
     */
    public function createOrder(User $user, Carrier $carrier, string $deliveryContent): Order
    {
        $date = new \DateTime();

        $order = new Order();
        $reference = $date->format('daY').'-'.uniqid();
        $order->setReference($reference);
        $order->setUser($user);
        $order->setCreatedAt($date);
        $order->setCarrierName($carrier->getName());
        $order->setCarrierPrice($carrier->getPrice());
        $order->setDelivery($deliveryContent);
        $order->setState(Order::IS_NOT_PAY);
        $this->entityManager->persist($order);

        return $order;
    }

    /**
     * @param Address $address
     *
     * @return string
     */
    public function getDeliveryMessage(Address $address): string
    {
        $deliveryMessage = $address->getFirstname().' '.$address->getLastname();
        $deliveryMessage .= '<br/>'.$address->getPhone();

        if ($address->getCompany()) {
            $deliveryMessage .= '<br/>'.$address->getCompany();
        }

        $deliveryMessage .= '<br/>'.$address->getAddress();
        $deliveryMessage .= '<br/>'.$address->getPostal().' '.$address->getCity();
        $deliveryMessage .= '<br/>'.$address->getCountry();

        return $deliveryMessage;
    }

    /**
     * @param Carrier|null $carrier
     * @param Address|null $address
     *
     * @return string|null
     */
    public function isValidOrderInformations(?Carrier $carrier, ?Address $address): ?string
    {
        if(!$address)
        {
            return 'Sélectionner une addresse';
        }

        if(!$carrier)
        {
            return 'Sélectionner un livreur';
        }

        $carrierExist = $this->entityManager->getRepository(Carrier::class)->find($carrier->getId());

        if(!$carrierExist)
        {
            return 'Sélectionner un livreur valide';
        }

        return null;
    }
}