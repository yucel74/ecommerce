<?php

namespace App\Service;

use App\Entity\ResetPassword;
use App\Entity\User;
use App\Repository\ResetPasswordRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class ResetPasswordService
{
    private $entityManager;

    private $resetPasswordRepository;

    private $containerBag;

    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerBagInterface $containerBag,
        ResetPasswordRepository $resetPasswordRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->containerBag = $containerBag;
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function getByToken(string $token)
    {
        return $this->resetPasswordRepository->findOneByToken($token);
    }

    /**
     * @param User $user
     *
     * @return ResetPassword|null
     */
    public function createResetPassword(User $user): ?ResetPassword
    {
        $resetPassword = new ResetPassword();
        $resetPassword->setUser($user);
        $resetPassword->setToken(uniqid());
        $resetPassword->setCreatedAt(new \DateTime());
        $this->entityManager->persist($resetPassword);
        $this->entityManager->flush();

        return $resetPassword;
    }

    /**
     * @param string $userFirstName
     * @param string $resetPasswordUrl
     *
     * @return string
     */
    public function getPasswordResetRequestMessage(string $userFirstName, string $resetPasswordUrl): string
    {
        return "Bonjour " . $userFirstName . "<br/>
            Vous avez demandé à réinitialiser votre mot de passe sur le site Clothes.com.<br/><br/> 
            Merci de bien vouloir cliquer sur le lien suivant pour <a href='" . $resetPasswordUrl . "'>mettre à jour votre mot de passe</a>";
    }
}