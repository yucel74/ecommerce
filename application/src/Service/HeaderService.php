<?php

namespace App\Service;

use App\Repository\HeaderRepository;

class HeaderService {
    private $headerRepository;

    public function __construct(HeaderRepository $headerRepository)
    {
        $this->headerRepository = $headerRepository;
    }

    public function getHeaders(): array
    {
        return $this->headerRepository->findAll();
    }
}