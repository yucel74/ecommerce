<?php

namespace App\Service;

use App\Entity\Product;
use App\Classe\Cart;

class CartService {

    /**
     * @param Product $productSearch
     * @param Cart    $cart
     *
     * @return int
     */
    public function getProductCount(Product $productSearch, Cart $cart): int
    {
        foreach($cart->getFull() as $product) {
            if($product['product'] === $productSearch) {
                return $product['quantity'];
            }
        }
       
        return false;
    }
}