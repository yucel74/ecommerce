<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductService {
    private $productRepository;
    private $entityManager;

    public function __construct(
        ProductRepository $productRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getProduct(int $id)
    {
        return $this->productRepository->findOneById($id);
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->productRepository->findAll();
    }

    /**
     * collects the best-selling products
     * @param int $id
     *
     * @return mixed
     */
    public function getBestSelling(int $id)
    {
        return $this->productRepository->findByIsBest($id);
    }

    /**
     * @param string $name
     *
     * @return Product|null
     */
    public function getProductByName(string $name): ?Product
    {
        return $this->productRepository->findOneByName($name);
    }
}