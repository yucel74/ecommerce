<?php

namespace App\Service;

use App\Entity\Address;
use App\Repository\AddressRepository;

class AddressService {

    private $addressRepository;

    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param int $id
     *
     * @return Address|null
     */
    public function getAddress(int $id): ?Address
    {
        return $this->addressRepository->findOneById($id);
    }
}