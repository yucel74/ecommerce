<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderDetails;
use Doctrine\ORM\EntityManagerInterface;

class OrderDetailsService {

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Order $order
     * @param array $productDetails
     *
     * @return OrderDetails
     */
    public function createOrderDetails(Order $order, array $productDetails): OrderDetails
    {
        $orderDetails = new OrderDetails();
        $orderDetails->setMyOrder($order);
        $orderDetails->setProduct($productDetails['product']->getName());
        $orderDetails->setQuantity($productDetails['quantity']);
        $orderDetails->setPrice($productDetails['product']->getPrice());
        $orderDetails->setTotal($productDetails['product']->getPrice() * $productDetails['quantity']);
        $this->entityManager->persist($orderDetails);

        return $orderDetails;
    }
}