describe('Test du formulaire de connexion', () => {
    beforeEach(() => {
        cy.visit('http://127.0.0.1:8741/connexion')
      })

      it('Connexion avec mail et mauvais mot de passe', () => {
        cy.get('#inputEmail')   
        .type('pauljacques@gmail.com')
        cy.get('#inputPassword')   
        .type('Mauvaismotdepasse1!{enter}')
        cy.get('.alert-danger')   
        .should('have.text', 'Identifiants invalides.')   
        
    })

    it('Connexion avec mail inconnu', () => {
        cy.get('#inputEmail')
            .type('sophie@gmail.com')
        cy.get('#inputPassword')
            .type('Mauvaismotdepasse1!{enter}')
        cy.get('.alert-danger')
            .should('have.text', 'L\'email entré n\'est pas connu.')

    })

    it('Connexion avec mail et mot de passe et déconnexion', () => {
        cy.get('#inputEmail')   
        .type('pauljacques@gmail.com')
        cy.get('#inputPassword')   
        .type('Leclavier1!{enter}')
        cy.get('h1')   
        .should('have.text', 'Mon compte')
        cy.url()
        .should('include', '/compte')            
        cy.get('#logout').click()
        cy.get('#login').click()
        cy.get('h1')   
        .should('have.text', 'Veuillez-vous connecter')
    }) 
})

