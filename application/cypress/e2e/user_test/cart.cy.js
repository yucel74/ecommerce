describe('Test du panier', () => {
    it('Ajout et validation du Panier', () => {
        // cy.get('.product-item > a:first').click()
        cy.visit('http://127.0.0.1:8741/nos-produits')
        //selctionne le prduit ayant pour id 4, clique dessus
        cy.get('#product-12').click()
        //clique une fois sur le bouton ajouter au panier
        cy.get('.btn-primary').click()
        //vérification du prix lorsque le produit s'affiche seul, une fois avoir cliqué dessus depuis la page catalogue
        cy.get('.product-page-price').should('have.text', '9,00 €')
        //Vérification del'indication du panier pour savoir si mon produit c'est bien ajouter
        cy.get('#info-quantity').should('have.text', ' Quantité dans votre panier 1')
        //vérification que un input est mis en place suite à mon clique d'ajout au panier, et contient 1 par défaut
        cy.get('.form-control').should('have.value', '1')
        // j'efface le contenue de input
        cy.get('.form-control').clear()
        //j'écris dans le input une chaine de caractère pour voir si cela modifie ce que j'ai dans le panier
        cy.get('.form-control').type('test chaine de caractère')
        //je valide avec le bouton ajouter au panier
        cy.get('.btn-primary').click()
        //Je vérifie que mon panier a bien la quantité 1 (pour savoir si la chaine a eu impact sur la quantité du produit dans le panier)
        cy.get('#info-quantity').should('have.text', ' Quantité dans votre panier 1')
        //j'efface le contenu de input
        cy.get('.form-control').clear()
        // je sais 2
        cy.get('.form-control').type('2')
        //je valide avec le bouton ajouter au panier
        cy.get('.btn-primary').click()
        //Je verifie que maintenat la quantité est de 3 puisque je l'ai jouter une fois puis deux fois
        cy.get('#info-quantity').should('have.text', ' Quantité dans votre panier 3')
        //je clique sur mon panier
        cy.get('.cart-icon').click()
        //je vérifie que dans le récapitulatif mon produit est bien le bon
        cy.get('#product-12')
        //je verifie la quantité
        cy.get('#info-quantity').should('have.text', 'x 3')
        // je verifie le prix à l'unité
        cy.get('#uniti-price').should('have.text', '9,00 €')
        // je verifie le prix total
        cy.get('#total-price').should('have.text', ' 27,00 €')
        //je verifie la quantité dans le petit récapitulatif en bas a droite
        cy.get('#info-quantity-recap').should('have.text', 'Nombre de produit(s):  3')
        //je verifie le prix total des produits dans le petit récapitulatif en bas a droite
        cy.get('#total-price-recap').should('have.text', 'Prix de mon panier: 27,00 €')
        //je clique sur valider mon panier
        cy.get('.btn-block').click()
        //je verifie que j'arrive sur le formulaire de connnexion (ce qui est normal car un utilisateur non connecté
        //ne peut pas passer de commande)
        cy.url('include', '/connexion')
    })
})

