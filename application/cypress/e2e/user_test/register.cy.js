describe('Test du formulaire d\'inscription', () => {
    beforeEach(() => {
        cy.visit('http://127.0.0.1:8741/inscription')
      })
    var base_mail = "testmail"
    var mail = base_mail + Math.random() + "@gmail.com";

    it('Test avec un mot de passe qui ne contient pas de lettre majucule, de caractère spécial et pas de chiffre.', () => {
        
        cy.get('#register_firstname')   
        .type('Jean')
        cy.get('#register_lastname')   
        .type('Barreau')
        cy.get('#register_email')   
        .type(mail)
        cy.get('#register_password_first')   
        .type('concepteur')
        cy.get('#register_password_second')   
        .type('concepteur')
        cy.get('#register_submit')   
        .click()
        cy.get('.form-error-message')
        .should('have.text', "Le mot de passe doit contenir au minimum 8 caractères dont : 1 miniscule, 1 majuscule, un chiffre et un caractère spécial.")
    })

    it('Les mots de passe ne sont pas identiques', () => {
        
        cy.get('#register_firstname')   
        .type('Jean')
        cy.get('#register_lastname')   
        .type('Barreau')
        cy.get('#register_email')   
        .type(mail)
        cy.get('#register_password_first')   
        .type('Savoir1!')
        cy.get('#register_password_second')   
        .type('Savoir1!!')
        cy.get('#register_submit')   
        .click()
        cy.get('.form-error-message')
        .should('have.text', "Le mot de passe doit être identique")
    })


    it('Inscription avec un mot de passe conforme', () => {
        
        cy.get('#register_firstname')   
        .type('Jean')
        cy.get('#register_lastname')   
        .type('Barreau')
        cy.get('#register_email')   
        .type(mail)
        cy.get('#register_password_first')   
        .type('Savoir1!')
        cy.get('#register_password_second')   
        .type('Savoir1!')
        cy.get('#register_submit')   
        .click()
        cy.get('.alert-info')
        .should('have.text', "Votre inscription s'est bien déroulée. Vous pouvez dès à présent vous connecter à votre compte.")
    })

    it('Le mail enregistré dans le test précédent ne peut être utilisé pour une autre inscription (le mail est unique pour chaque inscription)', () => {
        
        cy.get('#register_firstname')   
        .type('Jean')
        cy.get('#register_lastname')   
        .type('Barreau')
        cy.get('#register_email')   
        .type(mail)
        cy.get('#register_password_first')   
        .type('Motdepasse1!')
        cy.get('#register_password_second')   
        .type('Motdepasse1!')
        cy.get('#register_submit')   
        .click()
        cy.get('.alert-info')
        .should('have.text', "L'email que vous avez renseigné est déjà utilisé.")
    })

    
  
})





