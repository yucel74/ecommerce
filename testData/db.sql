-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 18 oct. 2021 à 22:09
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `clothes`
--
USE clothes;
-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE `address` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `address`
--

INSERT INTO `address` (`id`, `user_id`, `name`, `firstname`, `lastname`, `company`, `address`, `postal`, `city`, `country`, `phone`) VALUES
(1, 1, 'Maison', 'Paul', 'Jacques', NULL, '1 Avenue des palaces', '75008', 'Paris', 'FR', '0652489562');

-- --------------------------------------------------------

--
-- Structure de la table `carrier`
--

CREATE TABLE `carrier` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `carrier`
--

INSERT INTO `carrier` (`id`, `name`, `description`, `price`) VALUES
(1, 'Colissimo', 'Votre colis est chez vous en 48h!', 399),
(2, 'Chronopost', 'Votre colis est dans vos mains en 24h!', 499);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(29, 'Bonnets'),
(30, 'Echarpes'),
(31, 'Manteaux'),
(32, 'T-shirts');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210510164311', '2021-09-27 02:01:44', 378),
('DoctrineMigrations\\Version20210511162203', '2021-09-27 02:01:44', 7),
('DoctrineMigrations\\Version20210514065830', '2021-09-27 02:01:44', 18),
('DoctrineMigrations\\Version20210514120146', '2021-09-27 02:01:44', 60),
('DoctrineMigrations\\Version20210623140443', '2021-09-27 02:01:45', 61),
('DoctrineMigrations\\Version20210627214024', '2021-09-27 02:01:45', 16),
('DoctrineMigrations\\Version20210627221607', '2021-09-27 02:01:45', 114),
('DoctrineMigrations\\Version20210629094533', '2021-09-27 02:01:45', 6),
('DoctrineMigrations\\Version20210704091714', '2021-09-27 02:01:45', 5),
('DoctrineMigrations\\Version20210704124227', '2021-09-27 02:01:45', 6),
('DoctrineMigrations\\Version20210706173809', '2021-09-27 02:01:45', 5),
('DoctrineMigrations\\Version20210722114327', '2021-09-27 02:01:45', 7),
('DoctrineMigrations\\Version20210723161244', '2021-09-27 02:01:45', 16),
('DoctrineMigrations\\Version20210727235955', '2021-09-27 02:01:45', 57);

-- --------------------------------------------------------

--
-- Structure de la table `header`
--

CREATE TABLE `header` (
  `id` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `btn_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `btn_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `illustration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `header`
--

INSERT INTO `header` (`id`, `title`, `content`, `btn_title`, `btn_url`, `illustration`) VALUES
(1, 'Découvre la robe du moment!', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 'Découvrir la robe', 'http://127.0.0.1:8741/produit/9', 'dc5e2c80fb2b19062147762aaa299438a50d1111.jpg'),
(2, 'Il me manque plus que l\'écharpe !', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 'Découvrir l\'écharpe', 'http://127.0.0.1:8741/produit/7', '1100b8b54f32f4fc930602fa9a5082787432382f.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE `order` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` datetime NOT NULL,
  `carrier_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `carrier_price` double NOT NULL,
  `delivery` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `order`
--

INSERT INTO `order` (`id`, `user_id`, `created_at`, `carrier_name`, `carrier_price`, `delivery`, `reference`, `stripe_session_id`, `state`) VALUES
(1, 1, '2024-07-05 16:06:51', 'Colissimo', 399, 'Paul Jacques<br/>0652489562<br/>1 Avenue des palaces<br/>75008 Paris<br/>FR', '05pm2024-66881a1b93368', 'cs_test_b1xQWpuCjiCWkzxBcb5fFYhSFmavZzGPz7xaxcPTIH8Fbiw6r1Z0bk0Asl', 1);

-- --------------------------------------------------------

--
-- Structure de la table `order_details`
--

CREATE TABLE `order_details` (
  `id` int NOT NULL,
  `my_order_id` int NOT NULL,
  `product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `price` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `order_details`
--

INSERT INTO `order_details` (`id`, `my_order_id`, `product`, `quantity`, `price`, `total`) VALUES
(1, 1, 'Le bonnet du skieur', 4, 1200, 4800),
(2, 1, 'T-shirt blanc', 1, 1100, 1100);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `category_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `illustration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `is_best` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `slug`, `illustration`, `subtitle`, `description`, `price`, `is_best`) VALUES
(5, 29, 'Bonnet rouge', 'bonnet-rouge', '83d2f0b2049388ea04e4b006669436984b2e36f5.jpg', 'Le bonnet parfait pour l\'hiver', 'Bonnet Rouge Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 1500, 0),
(6, 29, 'Le bonnet du skieur', 'le-bonnet-du-skieur', 'f680ccbd62562a7c20610bc0d9f5f9e0c5428f53.jpg', 'Le bonnet parfait pour le ski', 'Bonnet adapté pour le ski! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 1200, 1),
(7, 30, 'L\'écharpe du loveur', 'lecharpe-du-loveur', '8aa5b3d4d21d3499067aa17702fb8690ad1988d1.jpg', 'L\'écharpe parfait pour les soirées romantiques', 'Echarpe très Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 1600, 1),
(8, 30, 'écharpe rouge de toulouse', 'lecharpe-du-samedi-soir', '18a0ddb40c0e5ddadee21be8291f8380bc215c7d.jpg', 'L\'écharpe pour les week-end', 'Echarpe rouge Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 1400, 1),
(9, 31, 'Manteau de soirée', 'le-manteau-de-soiree', 'c62946661a7dc0b9310fc2396d69aebcfe3bf72f.jpg', 'sdfghjk', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 6500, 0),
(10, 31, 'Manteau du dimanche', 'le-manteau-du-dimanche', '32c804084dee32ead1ca958e33c34d815b4abcab.jpg', 'Manteau avec couleur', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 4500, 0),
(11, 32, 'T-shirt blanc', 'T-shirt light', '962d177907092ac14e8d871cd8999eb8dbc0bfde.jpg', 't-shirt-light', 'T-shirt blanc', 1100, 0),
(12, 32, 'T-shirt noir', 'T-shirt dark', '0fc1fb618bf248418921e212d8ad2e3d4c896f70.jpg', 'T-shirt-dark', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lacinia nunc. Sed id gravida odio. Proin mauris risus, dapibus at purus vel, ultrices egestas enim. Integer molestie orci vel hendrerit pharetra. Etiam egestas nibh a libero sollicitudin, sed laoreet justo bibendum. Quisque ac tortor at sem placerat rutrum.', 900, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reset_password`
--

CREATE TABLE `reset_password` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `firstname`, `lastname`) VALUES
(1, 'pauljacques@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$I3MZeQPyxZB77qLK0ITxuQ$KI5nKyPBG0wV72sameguC/Q5Igb6uEorklRn09aPnEo', 'Paul', 'Jacques'),
(2, 'johndoe@gmail.com', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$039PMhdeyNplNJpfhlNATQ$2IWgyuSH2a2rtv5J/eOPAwcRolUqYgs0fiVqBS/iyw8', 'John', 'Doe');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D4E6F81A76ED395` (`user_id`);

--
-- Index pour la table `carrier`
--
ALTER TABLE `carrier`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5299398A76ED395` (`user_id`);

--
-- Index pour la table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_845CA2C1BFCDF877` (`my_order_id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04AD12469DE2` (`category_id`);

--
-- Index pour la table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B9983CE5A76ED395` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `address`
--
ALTER TABLE `address`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `carrier`
--
ALTER TABLE `carrier`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `header`
--
ALTER TABLE `header`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `order`
--
ALTER TABLE `order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `FK_D4E6F81A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK_845CA2C1BFCDF877` FOREIGN KEY (`my_order_id`) REFERENCES `order` (`id`);

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `reset_password`
--
ALTER TABLE `reset_password`
  ADD CONSTRAINT `FK_B9983CE5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
