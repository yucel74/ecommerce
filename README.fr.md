[![en](https://img.shields.io/badge/lang-en-red.svg)](https://bitbucket.org/yucel74/ecommerce/src/master/README.md)

## Installation
Utilisation de **php 7.4** et de symfony **5.2**

1. Télécharger le projet:  
- git clone https://yucel74@bitbucket.org/yucel74/ecommerce.git
2. Une fois le projet ecommerce installé, exécuter la commande:  
- cd ecommerce/application
3. Exécuter la commande:  
- composer install
4. Démarrer les conteneurs Docker:  
- docker compose up -d
5. Entrer dans le conteneur Docker PHP avec la commande suivante:  
- docker exec -it www_docker_symfony bash
6. Ensuite exécuter:  
- cd application
7. Créer la base de données en exécutant:  
- php bin/console doctrine:database:create
8. Sortir du conteneur www_docker_symfony avec la commande:  
- exit
9. Entrer dans le conteneur Docker de la base de données avec la commande suivante:  
- docker exec -it db_docker_symfony bash
10. Importer la base de données avec la commande suivante:  
- mysql -u root -p < var/data/scripts/db.sql
11. Mettre root comme mot de passe.  

L'installation du projet est terminée.

Pour information la base de données est visible sur phpMyAdmin au lien suivant:
- http://127.0.0.1:8080/

## Compte utilisateur disponible après avoir **importé db.sql**  
**Compte client:**  
mail : pauljacques@gmail.com | mot de passe: Leclavier1!

**Compte administrateur:**  
mail : johndoe@gmail.com | mot de passe: Lasouris1!  

Lien de la page Accueil du site: http://127.0.0.1:8741

Pour administrer le site web (ajouter des produits, changer le statut d'une commande, etc):  
1. Se connecter en tant que admin (mail : johndoe@gmail.com, mot de passe : Lasouris1!)  
2. Aller dans l'url http://127.0.0.1:8741/admin dans lequel il est possible de créer, visualiser, modifier et supprimer les éléments suivants:  
- Utilisateur  
- Commande  
- Catégorie  
- Produit  
- Prestataire de transport  
- Bannière de la page d'accueil

## Tests Cypress dans le Projet

Dans le cadre de ce projet, des tests automatisés ont été mis en place en utilisant Cypress afin de valider plusieurs fonctionnalités essentielles. Voici un aperçu détaillé des tests effectués :

- **Panier** (Cart.cy.js)
  Scénario de test :
    - Ajout et validation du panier.


- **Connexion** (Login.cy.js)
  Scénarios de test :
    - Connexion avec un email valide et un mot de passe incorrect. 
    - Connexion avec un email inconnu.
    - Connexion avec un email et un mot de passe valides, suivi d'une déconnexion.  
  

- **Inscription** (Register.cy.js)
  Scénarios de test :
    - Validation d'un mot de passe qui ne contient pas de lettre majuscule, de caractère spécial, ni de chiffre.
    - Vérification lorsque le mot de passe et sa confirmation ne sont pas identiques lors de l'inscription.
    - Inscription avec un mot de passe conforme aux critères.
    - Vérification qu'un email enregistré précédemment ne peut être réutilisé pour une autre inscription (unicité de l'email).

Pour exécuter ces tests, depuis le répertoire ecommerce/application, utilisez la commande suivante :  
- npx cypress open

Une fenêtre s'ouvrira, où vous pourrez sélectionner et exécuter l'un des tests visibles dans l'interface.  

![img.png](cypress-readme.png)