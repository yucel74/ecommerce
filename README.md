[![fr](https://img.shields.io/badge/lang-fr-green.svg)](https://bitbucket.org/yucel74/ecommerce/src/master/README.fr.md)

## Installation
Using **php 7.4** and symfony **5.2**

1. Download the project:  
- git clone https://yucel74@bitbucket.org/yucel74/ecommerce.git
2. Once the ecommerce project is installed, run the command:  
- cd ecommerce/application
3. Run the command:  
- composer install
4. Start the Docker containers:  
- docker compose up -d
5. Enter the PHP Docker container with the following command:  
- docker exec -it www_docker_symfony bash
6. Then execute:  
- cd application
7. Create the database by running:  
- php bin/console doctrine:database:create
8. Exit the www_docker_symfony container with the command:  
- exit
9. Enter the database Docker container with the following command:  
- docker exec -it db_docker_symfony bash
10. Import the database with the following command:  
- mysql -u root -p < var/data/scripts/db.sql
11. Set root to password

The project installation is complete.

For information, the database is visible on phpMyAdmin at the following link:
- http://127.0.0.1:8080/

## User account available after **importing db.sql**  
**Customer account:**  
mail: pauljacques@gmail.com | password: Leclavier1!

**Administrator account:**  
mail: johndoe@gmail.com | password: Lasouris1!

Home page link: http://127.0.0.1:8741

To administer the website (add products, change order status, etc):  
1. Login as admin (email: johndoe@gmail.com, password: Lasouris1!)  
2. Go to http://127.0.0.1:8741/admin where you can create, view, edit and delete the following:  
- User  
- Ordering  
- Category  
- Product  
- Transport provider  
- Home page banner

## Cypress Tests in the Project

In this project, automated tests have been implemented using Cypress to validate several key functionalities. Here's a detailed overview of the tests conducted:

- **Cart** (Cart.cy.js)
  Test Scenario:
    - Adding and validating the cart.


- **Login** (Login.cy.js)
  Test Scenarios:
    - Login with a valid email and incorrect password.
    - Login with an unknown email.
    - Login with valid email and password, followed by logout.


- **Register** (Register.cy.js)
  Test Scenarios:
    - Validation of a password that lacks uppercase letters, special characters, or numbers.
    - Verification when password and its confirmation do not match during registration.
    - Registration with a password that meets criteria.
    - Ensuring an email previously registered cannot be reused for another registration (email uniqueness).

To execute these tests, navigate to the ecommerce/application directory and run the following command:  
- npx cypress open

A window will open where you can select and run any of the visible tests.  

![img.png](cypress-readme.png)